// Production steps of ECMA-262, Edition 6, 22.1.2.1
if (!Array.from) {
  Array.from = (function () {
    var symbolIterator;
    try {
      symbolIterator = Symbol.iterator
        ? Symbol.iterator
        : "Symbol(Symbol.iterator)";
    } catch (e) {
      symbolIterator = "Symbol(Symbol.iterator)";
    }

    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === "function" || toStr.call(fn) === "[object Function]";
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) return 0;
      if (number === 0 || !isFinite(number)) return number;
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    var setGetItemHandler = function setGetItemHandler(isIterator, items) {
      var iterator = isIterator && items[symbolIterator]();
      return function getItem(k) {
        return isIterator ? iterator.next() : items[k];
      };
    };

    var getArray = function getArray(T, A, len, getItem, isIterator, mapFn) {
      // 16. Let k be 0.
      var k = 0;

      // 17. Repeat, while k < len… or while iterator is done (also steps a - h)
      while (k < len || isIterator) {
        var item = getItem(k);
        var kValue = isIterator ? item.value : item;

        if (isIterator && item.done) {
          return A;
        } else {
          if (mapFn) {
            A[k] =
              typeof T === "undefined"
                ? mapFn(kValue, k)
                : mapFn.call(T, kValue, k);
          } else {
            A[k] = kValue;
          }
        }
        k += 1;
      }

      if (isIterator) {
        throw new TypeError(
          "Array.from: provided arrayLike or iterator has length more then 2 ** 52 - 1"
        );
      } else {
        A.length = len;
      }

      return A;
    };

    // The length property of the from method is 1.
    return function from(arrayLikeOrIterator /*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLikeOrIterator).
      var items = Object(arrayLikeOrIterator);
      var isIterator = isCallable(items[symbolIterator]);

      // 3. ReturnIfAbrupt(items).
      if (arrayLikeOrIterator == null && !isIterator) {
        throw new TypeError(
          "Array.from requires an array-like object or iterator - not null or undefined"
        );
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== "undefined") {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError(
            "Array.from: when provided, the second argument must be a function"
          );
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      return getArray(
        T,
        A,
        len,
        setGetItemHandler(isIterator, items),
        isIterator,
        mapFn
      );
    };
  })();
}
// ^ used for IE compatibility

const form = document.querySelector(".main-right-form");
const allInputs = Array.from(form.querySelectorAll(".input-main-right"));
const allDropdowns = Array.from(
  document.querySelectorAll(".js-example-basic-single")
);
const submitButton = form.querySelector("#submitBtn");
const radioYes = form.querySelector("#yes");
const radioNo = form.querySelector("#no");
const gdprConsent = form.querySelector("#gdpr-consent");
let allValid;

$(document).ready(function () {
  // SELECT2 START

  $(function () {
    $("#anredeDropdown").select2({
      theme: "main",
      minimumResultsForSearch: -1,
    });
    $("#titelDropdown").select2({
      theme: "main",
      minimumResultsForSearch: -1,
    });
  });
  // validation on each change//
  $("#anredeDropdown").on("change", runFinalValidation);
  $("#titelDropdown").on("change", runFinalValidation);
  // validation on close //
  $("#anredeDropdown").on("select2:close", function (e) {
    checkSelectValue(e);
  });
  $("#titelDropdown").on("select2:close", function (e) {
    checkSelectValue(e);
  });

  // SELECT2 END

  // FLATPICKER START

  //   $("#dateOfBirth").flatpickr({
  //     altInput: true,
  //     altFormat: "F j, Y",
  //     dateFormat: "d-m-Y",
  //     disableMobile: true,
  //     // горното изключва native picker - като този, който се появява в chrome
  //     // nextArrow: '<i class="fas fa-chevron-right"></i>'
  // });

  $(".flatpickr-custom").flatpickr({
    wrap: true,
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "d-m-Y",
    disableMobile: true,
    nextArrow: '<i class="fas fa-chevron-right"></i>',
    prevArrow: '<i class="fas fa-chevron-left"></i>',
    locale: {
      firstDayOfWeek: 0,
      weekdays: {
        shorthand: ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'],
        longhand: ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'],         
      }
    }
  });
});
//add event listeners to all inputs
allInputs.forEach(function (input) {
  const label = input.previousElementSibling;

  input.addEventListener("focus", function (e) {
    label.classList.add("focused");
  });

  input.addEventListener("blur", function (e) {
    // if valid - proceed, if not - add error message
    if (validateOnBlur(e.target.name, e.target.value)) {
      if (input.classList.contains("invalid")) {
        input.classList.remove("invalid");
        input.style.borderBottom = "1.5px solid rgba(128, 128, 128, 0.63)";
        if (label.classList.contains("invalid")) {
          label.classList.remove("invalid");
        }
      }
      runFinalValidation();
      // run validation and change button color to orange if all are filled
    } else {
      label.classList.remove("focused");
      input.classList.add("invalid");
      input.style.borderBottom = "1px solid red";
      label.classList.add("invalid");
      runFinalValidation();
    }
  });
});

radioYes.addEventListener("click", runFinalValidation);
radioNo.addEventListener("click", runFinalValidation);
gdprConsent.addEventListener("click", runFinalValidation);

// function to enable button and change its color - only if all inputs are correct
function runFinalValidation() {
  allValid = true;
  // check all inputs
  allInputs.forEach(function (input) {
    if (input.classList.contains("invalid")) {
      submitButton.classList.add("disabled");
      allValid = false;
    }
  });
  // check all dropdowns

  if (allValid) {
    if (!checkDropdown()) {
      allValid = false;
    } else {
      allValid = true;
    }
  }
  // check all checkboxes
  if (allValid) {
    if (!checkCheckboxes()) {
      allValid = false;
    } else {
      allValid = true;
    }
  }

  if (allValid) {
    // log all input information
    submitButton.addEventListener("click", function (e) {
      e.preventDefault();
      console.log({
        anrede: form.querySelector("#anredeDropdown").value,
        titel: form.querySelector("#titelDropdown").value,
        firstName: form.querySelector("#firstName").value,
        lastName: form.querySelector("#lastName").value,
        dateOfBirth: form.querySelector("#dateOfBirth").value,
        email: form.querySelector("#email").value,
        companyName: form.querySelector("#companyName").value,
        street: form.querySelector("#street").value,
        houseNumber: form.querySelector("#houseNumber").value,
        postalCode: form.querySelector("#postalCode").value,
        location: form.querySelector("#location").value,
      });
    });

    submitButton.classList.remove("disabled");
    submitButton.classList.add("enabled");
  } else {
    submitButton.classList.remove("enabled");
    submitButton.classList.add("disabled");
  }
}
function validateOnBlur(name, value) {
  if (name === "firstName") {
    return checkIfOnlyLetters(value);
  } else if (name === "lastname") {
    return checkIfOnlyLetters(value);
  } else if (name === "houseNumber") {
    return checkIfOnlyDigits(value);
  } else if (name === "postalCode") {
    return checkIfOnlyDigits(value);
  } else if (name === "email") {
    return checkIfIsEmail(value);
  } else {
    return checkIfOnlyLetters(value);
  }
}

//helper functions
function checkIfOnlyLetters(value) {
  let pattern = /^[a-zA-Z]+$/;
  if (pattern.test(value) && value !== "") {
    return true;
  } else {
    return false;
  }
}
function checkIfOnlyDigits(value) {
  let pattern = /^[0-9]+$/;
  if (pattern.test(value) && value !== "") {
    return true;
  } else {
    return false;
  }
}
function checkIfIsEmail(value) {
  let pattern = /\S+@\S+\.\S+/;

  if (pattern.test(value) && value !== "") {
    return true;
  } else {
    return false;
  }
}
function checkDropdown(e) {
  const dropdown1Value = document.querySelector("#anredeDropdown").value;
  const dropdown2Value = document.querySelector("#titelDropdown").value;
  if (
    (dropdown1Value === "mr" || dropdown1Value === "mrs") &&
    (dropdown2Value === "nurse" ||
      dropdown2Value === "doctor" ||
      dropdown2Value === "pharmacist")
  ) {
    return true;
  } else {
    return false;
  }
}
function checkCheckboxes() {
  if (
    document.querySelector("#yes").checked &&
    document.querySelector("#gdpr-consent").checked
  ) {
    return true;
  } else {
    return false;
  }
}
function checkSelectValue(e) {
  let value = e.target.value;
  let dropdown = e.target.id;
  let arrowAnrede = document.querySelectorAll(
    ".select2-container--main .select2-selection--single .select2-selection__arrow b"
  )[0];
  let arrowTitel = document.querySelectorAll(
    ".select2-container--main .select2-selection--single .select2-selection__arrow b"
  )[1];
  if (dropdown === "anredeDropdown") {
    if ((value === "mr" || value === "mrs") && value !== "") {
      arrowAnrede.classList.remove("invalidArrow");
      document
        .querySelector("#select2-anredeDropdown-container")
        .classList.remove("invalidDropdown");
    } else {
      document
        .querySelector("#select2-anredeDropdown-container")
        .classList.add("invalidDropdown");
      arrowAnrede.classList.add("invalidArrow");
    }
  }
  if (dropdown === "titelDropdown") {
    if (
      (value === "nurse" || value === "doctor" || value === "pharmacist") &&
      value !== ""
    ) {
      arrowTitel.classList.remove("invalidArrow");
      document
        .querySelector("#select2-titelDropdown-container")
        .classList.remove("invalidDropdown");
    } else {
      document
        .querySelector("#select2-titelDropdown-container")
        .classList.add("invalidDropdown");
      arrowTitel.classList.add("invalidArrow");
    }
  }
}
